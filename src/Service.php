<?php

namespace liuwave\filesystem;

class Service extends \think\Service
{
    public function register(): void
    {
        $this->app->bind('filesystem', Filesystem::class);
    }
}
